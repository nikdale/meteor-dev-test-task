import { ReactiveDict } from 'meteor/reactive-dict'
import { EventEmitter } from '/utils/event-emitter'
import { SortableListChanges } from './sortable-list-changes'

export class SortableListState {
  constructor() {
    this._sortableItems = new Map()
    this._initialItems = new Map()
    this._listChanges = new SortableListChanges()
    this._eventEmitter = new EventEmitter()
    this._initialized = false
    this._setupIdGenerator()
    this.state = new ReactiveDict()
    this.state.set({
      ready: false,
      locked: false
    })
  }

  init(items) {
    this._initialized = false
    this._listChanges.clearAll()
    this._sortableItems.clear()
    this._initialItems.clear()
    items.forEach((item, index) => {
      const position = index + 1
      const itemId = this.addItem(item, position)
      this._initialItems.set(itemId, { position, item })
    })
    this._initialized = true
    this.state.set({ ready: true })
  }

  isReady() {
    return this.state.get('ready')
  }

  getItems() {
    return Array.from(this._sortableItems.values()).sort(
      (itemA, itemB) => itemA.position - itemB.position
    )
  }

  getItemsData(items = []) {
    return items.map(item => item.getCurrentData())
  }

  getItemsById(itemIds) {
    return itemIds.map(itemId => this._sortableItems.get(itemId))
  }

  getInitialItemsById(itemIds) {
    return itemIds.map(itemId => this._initialItems.get(itemId).item)
  }

  addItem(item, position) {
    const itemId = this._getId()
    item.itemId = itemId
    item.position = position
    this._sortableItems.set(itemId, item)
    if (this._initialized) {
      this._listChanges.added(itemId)
      this._eventEmitter.emit('addItem', item)
    }
    return itemId
  }

  deleteItem(item) {
    const itemId = item.itemId
    this._sortableItems.delete(itemId)
    if (this._initialized) {
      this._eventEmitter.emit('deleteItem', item)
      this._listChanges.deleted(itemId)
    }
  }

  updateItemsPosition(itemsPosition) {
    for (const itemId in itemsPosition) {
      const item = this._sortableItems.get(itemId)
      const newPosition = itemsPosition[itemId]
      item.position = newPosition
      if (this._itemPositionChanged(itemId, newPosition)) {
        this._listChanges.reordered(itemId)
      } else {
        this._listChanges.reorderedBack(itemId)
      }
    }
  }

  reset() {
    const items = []
    this._initialItems.forEach(({ item, position }) => {
      item.reset(position)
      items.push(item)
    })

    items.sort((itemA, itemB) => itemA.position - itemB.position)
    this.init(items)
    this._eventEmitter.emit('reset')
  }

  hasChanges() {
    return this._listChanges.hasChanges()
  }

  getChanges() {
    const {
      added,
      modified,
      reordered,
      deleted
    } = this._listChanges.getChanges()
    return {
      added: this.getItemsById(added),
      modified: this.getItemsById(modified),
      reordered: this.getItemsById(reordered),
      deleted: this.getInitialItemsById(deleted)
    }
  }

  itemModified(item) {
    this._listChanges.modified(item.itemId)
  }

  itemModifiedBack(item) {
    this._listChanges.modifiedBack(item.itemId)
  }

  on(eventName, listener) {
    return this._eventEmitter.addListener(eventName, listener)
  }

  off(eventName, listener) {
    return this._eventEmitter.removeListener(eventName, listener)
  }

  lock() {
    this.state.set({ locked: true })
  }

  unlock() {
    this.state.set({ locked: false })
  }

  isLocked() {
    return this.state.get('locked')
  }

  checkItemChanged(item = {}) {
    if (!this._sortableItems.has(item.itemId)) {
      return
    }

    if (item.hasChanges()) {
      this.itemModified(item)
    } else {
      this.itemModifiedBack(item)
    }
  }

  _setupIdGenerator() {
    let itemIdBase = Date.now()
    this._getId = function() {
      itemIdBase++
      return itemIdBase.toString()
    }
  }
  _itemPositionChanged(itemId, newPosition) {
    const initialItem = this._initialItems.get(itemId)
    if (!initialItem) {
      return true
    }
    return initialItem.position !== newPosition
  }
}
