import { Template } from 'meteor/templating'
import { StageListItemState } from './stage-list-item-state'

import '../form/form-input'
import '../icons/bin'
import '../icons/drag-handle'
import './stage-list-item.html'

const t = Template.stageListItem

t.onCreated(function() {
  this.handleSubmit = () => {
    const { item, list } = this.data
    const newStage = {
      name: item.stageName,
      number: item.position
    }

    const newItem = new StageListItemState({ stage: newStage })
    list.addItem(newItem, item.position)
    item.stageName = ''
  }
  this.handleRemoveItem = () => {
    const { list, item } = this.data
    list.deleteItem(item)
  }

  this.autorun(() => {
    const { item, list } = Template.currentData()
    list.checkItemChanged(item)
  })
})

t.helpers({
  stageNameInputOptions() {
    const { item } = this
    return {
      name: 'name',
      placeholder: 'Enter stage name',
      value: item.stageName,
      onChange: (inputName, value) => {
        item.stageName = value
      }
    }
  }
})

t.events({
  'submit .js-stage-item-form'(evt, instance) {
    evt.preventDefault()
    instance.handleSubmit()
  },
  'click .js-remove'(evt, instance) {
    evt.preventDefault()
    instance.handleRemoveItem()
  }
})
