import { ReactiveVar } from 'meteor/reactive-var'

export class SortableListItemState {
  constructor({ itemTemplateName, options = {} }) {
    this._initialOptions = { ...options, templateName: itemTemplateName }
    this._setupOptions(this._initialOptions)
  }

  get templateName() {
    return this._templateName
  }

  set position(value) {
    this._position.set(value)
  }

  get position() {
    return this._position.get()
  }

  reset(position) {
    const options = { ...this._initialOptions, position }
    this._setupOptions(options)
  }

  getCurrentData() {
    return { position: this.position, itemId: this.itemId }
  }

  hasChanges() {
    return false
  }

  _setupOptions(options = {}) {
    const { itemId, position, sortable = true, templateName } = options
    this.itemId = itemId
    this.sortable = sortable
    this._position = new ReactiveVar(position)
    this._templateName = templateName
  }
}
