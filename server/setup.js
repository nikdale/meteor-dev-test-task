import { Stages } from '/api/server/stages'

const initialStages = [
  { name: 'Stage one', number: 1 },
  { name: 'Stage two', number: 2 },
  { name: 'Stage three', number: 3 },
  { name: 'Stage four', number: 4 }
]

if (Stages.find().count() === 0) {
  initialStages.forEach(stage => Stages.insert(stage))
}
